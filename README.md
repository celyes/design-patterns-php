# PHP Design Patterns

The most well-known design patterns written in PHP 7.

### Install & configuration

install process is done using Composer:

```bash
composer require celyes/design-patterns
```

install dependencies:

```bash
composer install
```

You can also fork this repo and clone it.

### Contribution

any additions, improvements or modifications are welcome.

### Running test

```bash
vendor/bin/phpunit
```

### Issues

[See here](https://github.com/celyes/design-patterns-php/issues) 